"
  Permet d'appeler directement les 6 modèles différents sur une matrice X, 
  un vecteur d'étiquette z et un nombre N d'itération pour le calcul de l'erreur moyenne. 
"

err.6modeles <- function(X,z,N){
  err <- NULL
  err$adl <- errADL(X,z,N)
  print(err$adl)
  err$adq <- errADQ(X,z,N)
  print(err$adq)
  err$nba <- errNBA(X,z,N)
	print(err$nba)
  err$log0 <- errLOG(X,z,0,10e-5,N)
	print(err$log0)
  err$log1 <- errLOG(X,z,1,10e-5,N)
	print(err$log1)
  err$log20 <- errLOG2(X,z,0,10e-5,N)
	print(err$log20)
  err$log21 <- errLOG2(X,z,1,10e-5,N)
	print(err$log21)
  err$tree <- errTREE(X,z,N)
	print(err$tree)
  err
}
