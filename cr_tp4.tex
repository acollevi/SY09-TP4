\documentclass{article}
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{bbm}
\usepackage{subfig}
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry}
\usepackage{multirow}
\usepackage{multicol} % Style double colonne
\usepackage{abstract} % Customization de l'abstract
\usepackage{fancyhdr} % en-têtes et pieds de page
\usepackage{float} % Nécessaire pour les tables et figures dans l'environnement double colonne
\usepackage{array}

\usepackage[colorlinks=true,linkcolor=red,urlcolor=blue,filecolor=green]{hyperref} % hyperliens

% En-têtes et pieds de page
\pagestyle{fancy}
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{SY09 - TP4 \hfill Aude COLLEVILLE - Alexis MATHEY } % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

%\setlength{\parskip}{1ex} % espace entre paragraphes

\newcommand{\bsx}{\boldsymbol{x}}
\newcommand{\transp}{^{\mathrm{t}}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}


%----------------------------------------------------------------------------------------

\title{Discrimination}

\author{Alexis Mathey \& Aude Colleville}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Insert title

\thispagestyle{fancy} % All pages have headers and footers


%----------------------------------------------------------------------------------------
\section*{Introduction}
L'objectif du TP est d'appliquer différents modèles de discrimination à des jeux de données dans le cas binaire. Ainsi on développe des fonctions permettant l'apprentissage d'un modèle à partir d'un ensemble d'apprentissage dans un premier temps et la validation du modèle grâce à un ensemble de test.

\section{Implémentation}

\subsection{Analyse Discriminante}

Tout d'abord, nous nous proposons d'étudier trois modèles d'analyse discriminante :
\begin{itemize}
\item L'analyse discriminante quadratique
\item L'analyse discriminante linéaire
\item le classifieur bayésien naïf
\end{itemize}
Ces trois fonctions renvoie les paramètres du modèles (moyennes, proportions et matrices de covariance).

Pour développer les fonctions permettant l'apprentissage du modèle de ces méthodes, nous avons développé tout d'abord celle de l'analyse discriminante quadratique avec les paramètres du modèle :
\begin{equation*}
\widehat{\pi_{k}} = \dfrac{n_{k}}{n}
\end{equation*}
\begin{equation*}
\widehat{\mu_{k}} = \dfrac{1}{n_{k}}\sum_{i=1}^{n} z_{ik}x{i}
\end{equation*}
\begin{equation*}
\widehat{\Sigma_{k}} = \dfrac{1}{n_{k}}\sum_{i=1}^{n} z_{ik} (x_{i}-\widehat{\mu_{k}})(x_{i}-\widehat{\mu_{k}})^{t}
\end{equation*}


Pour le développement de la fonction permettant l'apprentissage du modèle de l'analyse discriminante linéaire, nous avons simplement appelé celle de l'analyse discriminante quadratique et remplacé les covariances par la somme des deux covariances multipliées par la proportion de leur classe respective. Ainsi, nous obtenons une covariance unique construite à partir des covariances de chacune des classes.

De même, pour le développement de la fonction permettant l'apprentissage du modèle du classifieur bayésien naïf, nous avons utilisé la fonction de l'analyse discriminante quadratique et multiplié les covariances par la matrice identité.

Enfin, nous avons réalisé la fonction permettant d'effectuer la validation d'un ensemble de test à partir du modèle créé par ces trois fonctions. Cette fonction renvoie les probabilités à posteriori de l'ensemble de test ainsi que les étiquettes prédites pour cet ensemble de test.


\subsection{Régression logistique}


Par la suite, nous nous proposons d'étudier la régression logistique linéaire et quadratique.

Pour ce faire, nous développons comme précédemment une fonction permettant l'apprentissage du modèle grâce à l'algorithme de Newton-Raphson et une permettant sa validation.

\subsubsection{Apprentissage}

Pour cela, nous utilisons les probabilités à posteriori:\\
\begin{equation*}
P(w_{1}|x) = p(x,w) = \dfrac{\exp(w^{t}x)}{1+\exp(w^{t}x)}
\end{equation*}
\begin{equation*}
P(w_{2}|x) = 1-p(x,w)
\end{equation*}

La méthode de Newton-Raphson consiste à sélectionner un vecteur de poids initial $w^{(0)}$, puis à calculer une séquence de vecteurs $w^{(1)}$, $w^{(2)}$,... en appliquant de façon itérative la formule (\ref{eq:newton-raphson}) avec la matrice hessienne de la log-vraissemblance de Log L, calculée en $w^{(q)}$ (Formule \ref{eq:matrice-hessienne}) et le gradient de la log-vraissemblance calculée en $w^{(q)}$ (Formule \ref{eq:matrice-gradient}).

\begin{equation} \label{eq:newton-raphson}
w^{(k+1)} = w^{(q)}-H^{-1}_{(q)}*\dfrac{\partial \log L}{\partial w}(w^{(q)})
\end{equation}
\begin{equation} \label{eq:matrice-hessienne}
H_{(q)} = -X^{T}W_(q)X
\end{equation}
\begin{equation} \label{eq:matrice-gradient}
   \dfrac{\partial \log L}{\partial w}(w^{(q)}) = X^{T}(t-p^(q))
\end{equation}


On arrête l'itération lorsque la nouvelle estimation de la distance entre $w^{(q+1)}$ et $w^{(q)}$ est inférieure à un palier $\epsilon$ fixé (à $10e^{-5}$ par exemple).

Pour palier à la possibilité d'un vecteur X nul qui engendrerait un résultat $NULL$, on permet l'ajout d'une ordonnée à l'origine à $w^{(0)}$ pour rajouter de la flexibilité au modèle.\\

La fonction retourne donc la matrice beta correspondant à l'estimateur du maximum de vraisemblance $\widehat{\beta}$ des paramètres, le nombre \texttt{niter} d'itérations effectuées par l'algorithme de Newton-Raphson, et la valeur $\log L$ de la vraisemblance à l'optimum.

\subsubsection{Validation}

Nous créons ensuite la fonction de validation du modèle en calculant les probabilités à posteriori d'un ensemble de test à partir de la matrice beta du modèle appris. Pour chaque enregistrement, nous prédisons son étiquette en comparant les probabilités à posteriori de chaque classe. La fonction renvoie finalement la matrice des probabilités à posteriori ainsi que le vecteur des étiquettes prédites.

\subsubsection{Régression logistique quadratique}

Il est possible de généraliser le modèle de régression logistique linéaire dans un espace plus complexe dans lesquelles les classes peuvent être séparées par un hyperplan dans lequel sera effectuée la régression logistique. Ce modèle sera appelé régression logistique quadratique.

Dans notre cas, les individus sont décrits par les variables naturelles $X^{1}$ et $X^{2}$. Nous complétons le modèle avec des combinaisons de ces variables afin d'obtenir (\ref{eq:chi2}).
\begin{equation} \label{eq:chi2}
\chi^{2}=\left\{X^{1},X^{2},X^{1}X^{2},(X^{1})^{2},(X^{2})^{2}\right\}
\end{equation}

Nous avons donc créé une fonction qui permet de créer la matrice complétée $\chi^{2}$ à partir d'une matrice $\chi$. La régression logistique quadratique est ensuite réalisée à l'aide des méthodes de la régression logistique appliquée à nos nouvelles variables.


\section{Test sur données simulées }


Nous souhaitons tester les performances de chacun des modèles développé précédemment sur des ensembles de données simulés. Pour cela, nous mesurons l'erreur moyenne de chaque modèle sur une série de partitions du jeu de données.

Nous répétons donc $N=20$ fois le protocole suivant :
\begin{itemize}
\item séparation aléatoire du jeu de données en un ensemble d'apprentissage et un ensemble de test avec la fonction fournie \texttt{separ1},
\item apprentissage d'un modèle sur l'ensemble d'apprentissage,
\item classement des données de test et calcul du taux d'erreur associé.
\end{itemize}
Finalement, nous calculons le taux d'erreur moyen sur les 20 itérations. \\

Nous créons également une fonction permettant de tester les performances des arbres binaires de décision sur les données. Pour cela nous utilisons la fonction \texttt{tree} pour l'apprentissage et la fonction \texttt{predict} couplée à la fonction \texttt{which.max} pour la validation.\\

Les sections suivantes présentent les résultats des tests de performance sur les 3 jeux de données simulées : \texttt{Synth1-1000}, \texttt{Synth2-1000} et \texttt{Synth3-1000}.
\subsection{Tests sur Synth1-1000}
Dans cette partie, nous vérifions les performances des différents modèles sur le jeu de données \texttt{Synth1-1000}.\\
\begin{table}[H]
\begin{center}
\begin{tabular}{c|c}
Modèle&Taux d'erreur\\
\hline

ADQ&2,47\%\\
ADL&3,56\%\\
NBA&3,30\%\\
LOG (intr=0)&3,86\%\\
LOG (intr=1)&2,31\%\\
LOG2 (intr=0)&3,47\%\\
LOG2 (intr=1)&2,63\%\\
Arbre&3,83\%\\
\end{tabular}
\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{Synth1-1000}}
\end{center}
\end{table}

\begin{figure}[H]
    \centering
{{\includegraphics[width=4cm]{img/frontiereSynth1ADQ} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth1ADL} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth1NBA} }}%
    \caption{Frontière de décision de l'analyse discriminante}%
    \label{fig:ADSynth1}
\end{figure}

\begin{figure}[H]
    \centering
{{\includegraphics[width=3cm]{img/frontiereSynth1LOG0} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth1LOG1} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth1LOG20} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth1LOG21} }}%
    \caption{Frontière de décision de la régression logistique}%
    \label{fig:RLSynth1}
\end{figure}

Nous pouvons remarquer que les modèles offrant les taux d'erreur les plus bas sont l'ADQ et les régressions logistiques avec ordonnée à l'origine.

La performance de l'ADQ s'explique par le fait que chacune des classes de nos données suivent bien des distributions normales. L'ADL n'a en revanche pas donné de bons résultats car nos deux classes n'ont pas la même matrice de variance $\Sigma$ (voir Figure \ref{fig:ADSynth1}).

Les régressions logistiques avec ordonnée à l'origine sont également plus performantes car nos deux classes ne sont pas réparties autour de l'origine mais plutôt du point $(1,1)$.\textsl{}


\subsection{Tests sur Synth2-1000}
Dans cette partie, nous vérifions les performances des différents modèles sur le jeu de données \texttt{Synth2-1000}.\\
\begin{table}[H]
	\begin{center}
		\begin{tabular}{c|c}
			Modèle&Taux d'erreur\\
			\hline
			ADQ&0,92\%\\
			ADL&0,97\%\\
			NBA&1,51\%\\
			LOG (intr=0)&1,08\%\\
			LOG (intr=1)&1,29\%\\
			LOG2 (intr=0)&1,24\%\\
			LOG2 (intr=1)&1,33\%\\
			Arbre&3,80\%\\
		\end{tabular}
		\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{Synth2-1000}}
	\end{center}
\end{table}

\begin{figure}[H]
    \centering
{{\includegraphics[width=4cm]{img/frontiereSynth2ADQ} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth2ADL} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth2NBA} }}%
    \caption{Frontière de décision de l'analyse discriminante}%
    \label{fig:ADSynth2}
\end{figure}
\begin{figure}[H]
    \centering
{{\includegraphics[width=3cm]{img/frontiereSynth2LOG0} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth2LOG1} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth2LOG20} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth2LOG21} }}%
    \caption{Frontière de décision de la régression logistique}%
    \label{fig:RLSynth2}
\end{figure}

Ce jeu de données semble être composé de deux classes ayant la même matrice de variance $\Sigma$ réparties autour de l'origine. C'est donc sans surprise que les modèles donnant une frontière linéaire sont les plus performants.

En effet, l'analyse discriminante linéaire repose sur les hypothèses que les deux classes suivent des distributions normales (ce qui est le cas par construction des données) et qu'elles aient la même matrice de variance $\Sigma$. Le jeu de données \texttt{Synth2-1000} correspond donc parfaitement à ces hypothèses.

La régression logistique qui trace une frontière de décision linéaire est également très performante pour les mêmes raisons.


\subsection{Tests sur Synth3-1000}
Dans cette partie, nous vérifions les performances des différents modèles sur le jeu de données \texttt{Synth3-1000}.\\
\begin{table}[H]
	\begin{center}
		\begin{tabular}{c|c}
			Modèle&Taux d'erreur\\
			\hline
			ADQ&1,21\%\\
			ADL&2,39\%\\
			NBA&1,04\%\\
			LOG (intr=0)&2,4\%\\
			LOG (intr=1)&2,06\%\\
			LOG2 (intr=0)&1,43\%\\
			LOG2 (intr=1)&1,64\%\\
			Arbre&1,91\%\\
		\end{tabular}
		\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{Synth3-1000}}
	\end{center}
\end{table}

\begin{figure}[H]
    \centering
{{\includegraphics[width=4cm]{img/frontiereSynth3ADQ} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth3ADL} }}%
    \qquad
{{\includegraphics[width=4cm]{img/frontiereSynth3NBA} }}%
    \caption{Frontière de décision de l'analyse discriminante}%
    \label{fig:ADSynth3}
\end{figure}
\begin{figure}[H]
    \centering
{{\includegraphics[width=3cm]{img/frontiereSynth3LOG0} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth3LOG1} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth3LOG20} }}%
    \qquad
{{\includegraphics[width=3cm]{img/frontiereSynth3LOG21} }}%
    \caption{Frontière de décision de la régression logistique}%
    \label{fig:RLSynth3}
\end{figure}

Sur ce troisième jeu de données, les classifieurs les plus performants sont le classifieur bayésien naïf, l'analyse discriminante quadratique et la régression logistique quadratique sans ordonnée à l'origine.

Encore une fois, ces résultats ne sont pas surprenants. On voit en effet sur les graphes de la Figure \ref{fig:ADSynth3} que nos deux variables sont relativement indépendantes conditionnellement aux classes (la classe rouge s'étend selon l'axe V2 tandis que la classe verte s'étend selon l'axe V1). Ce cas correspondant aux hypothèses du classifieur Bayésien naïf, il n'est pas étonnant d'obtenir de bons résultats.

Pour ce qui est de l'analyse discriminante et la régression logistique quadratiques, leurs bonnes performances s'expliquent par leur capacité à "épouser" la forme de la séparation entre les classes. En effet, les deux classes ont des matrices $\Sigma$ bien distinctes rendant nécessaire l'utilisation de frontières quadratiques.

\section{Test sur données réelles}
Dans cette partie, on s'intéresse cette fois-ci à des données réelles dont on ne connait pas les distributions conditionnelles et on répète le protocole expérimental 100 fois.

\subsection{Pima}
Dans cette exercice, on considère un jeu de données traitant du diabète chez les individus d'une population d'amérindiens.
\begin{table}[H]
	\begin{center}
		\begin{tabular}{c|c}
			Modèle&Taux d'erreur\\
			\hline
			ADQ&24,04\%\\
			ADL&21,95\%\\
			NBA&23,79\%\\
			LOG (intr=0)&29,1\%\\
			LOG (intr=1)&21,67\%\\
			LOG2 (intr=0)&24,21\%\\
			LOG2 (intr=1)&24,1\%\\
			Arbre&26,98\%\\
		\end{tabular}
		\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{Pima}}
	\end{center}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=9cm]{img/plotPima}%
	\caption{Matrice de graphes des données Pima}%
	\label{fig:plotPima}
\end{figure}
Sur ce jeu de données, on remarque que les taux d'erreurs sont très élevés. En effet, le taux d'erreur est d'environ 20\% de plus que pour les données simulées. Le graphe des données Pima (figure \ref{fig:plotPima}) nous permet effectivement de constater que les classes ne sont pas distinctes et qu'aucune variable ne permet de distinguer la première classe de la deuxième. Cela rend la différenciation entre les classes difficile.

\subsection{Breast Cancer Wisconsin}

On considère à présent un problème de prédiction du niveau de gravité d'une tumeur à partir de descripteurs physiologiques.
\begin{table}[H]
	\begin{center}
		\begin{tabular}{c|c}
			Modèle&Taux d'erreur\\
			\hline
			ADQ&5,02\%\\
			ADL&4,54\%\\
			NBA&3,4\%\\
			LOG (intr=0)&15,86\%\\
			LOG (intr=1)&3,84\%\\
			Arbre&5,2\%\\
		\end{tabular}
		\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{BCW}}
	\end{center}
\end{table}
Pour ce jeu de données, les classifieurs les plus performants sont le classifieur bayésien naïf et la régression logistique avec ordonnée à l'origine.

Il n'est pas étonnant que la régression logistique soit plus performante avec une ordonnée à l'origine car toutes les variables prennent des valeurs entières entre 1 et 11 (Figure \ref{fig:summary-bcw}). La frontière tracée par la régression logistique classique ne pourra donc pas séparer correctement les deux classes. La régression logistique avec ordonnée à l'origine corrige ce problème.

D'autre part, les performances du classifieur bayésien naïf suggèrent une indépendance conditionnelle des variables du jeu de données.

\begin{figure}[H]
	\begin{center}
	\begin{verbatim}
	V2               V3               V4               V5              V6        
	Min.   : 1.000   Min.   : 1.000   Min.   : 1.000   Min.   : 1.00   Min.   : 1.000  
	1st Qu.: 2.000   1st Qu.: 1.000   1st Qu.: 1.000   1st Qu.: 1.00   1st Qu.: 2.000  
	Median : 4.000   Median : 1.000   Median : 1.000   Median : 1.00   Median : 2.000  
	Mean   : 4.442   Mean   : 3.151   Mean   : 3.215   Mean   : 2.83   Mean   : 3.234  
	3rd Qu.: 6.000   3rd Qu.: 5.000   3rd Qu.: 5.000   3rd Qu.: 4.00   3rd Qu.: 4.000  
	Max.   :10.000   Max.   :10.000   Max.   :10.000   Max.   :10.00   Max.   :10.000 
	
    V7               V8               V9             V10        
    Min.   : 2.000   Min.   : 1.000   Min.   : 1.00   Min.   : 1.000
    1st Qu.: 2.000   1st Qu.: 2.000   1st Qu.: 1.00   1st Qu.: 1.000
    Median : 2.000   Median : 3.000   Median : 1.00   Median : 1.000
    Mean   : 3.217   Mean   : 3.445   Mean   : 2.87   Mean   : 1.603
    3rd Qu.: 3.000   3rd Qu.: 5.000   3rd Qu.: 4.00   3rd Qu.: 1.000
    Max.   :11.000   Max.   :10.000   Max.   :10.00   Max.   :10.000
	       
	\end{verbatim}
	\caption{Résumé des variables du jeu de données BCW}
	\label{fig:summary-bcw}
	\end{center}
\end{figure}


\section{Challenge données SPAM}
On considère enfin un problème de détection de spams à partir d'indicateurs calculés sur des messages électroniques.
\begin{table}[H]
	\begin{center}
		\begin{tabular}{c|c}
			Modèle&Taux d'erreur\\
			\hline	
			ADQ&17,29\%\\
			ADL&11,31\%\\
			NBA&17,67\%\\
			LOG (intr=0)&8,5\%\\
			LOG (intr=1)&8,08\%\\
			Arbre&10,19\%
		\end{tabular}
		\caption{Erreurs moyennes des différents modèles sur le jeu de données \texttt{Spam}}
	\end{center}
\end{table}
On remarque ici que les taux d'erreurs sur ce jeu de données sont élevés, nous avons donc décidé d'utiliser les random Forest pour essayer d'obtenir un meilleur taux. Nous avons donc créé une fonction permettant de calculer le taux d'erreur moyen en faisant l'apprentissage du modèle grâce à la fonction \texttt{randomForest} et la validation grâce à la fonction \texttt{predict}. Avec ce modèle, on obtient une erreur moyenne sur 20 itérations de 3,03\%. Ce modèle montre donc de bonnes performances sur ce jeu de données.


\section*{Conclusion}
Finalement, ce TP nous a permis d'appliquer les différentes méthodes d'analyse discriminantes, de régression logistique et d'arbres de décision binaires sur des données et de tester leurs performances. Il est très intéressant de constater la différence entre l'application sur des données simulées (dont on connait la distributions conditionnelles des classes) et des données réelles pour lesquelles les performances des différents classifieurs varient beaucoup.
%----------------------------------------------------------------------------------------


\end{document}
